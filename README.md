<p align="center">
<img src="images/logo.png" align="center"  height="240">
</p>
<h1 align="center">SpaceBoxAi - Your AI Assistant</h1>

SpaceBoxAi is your best AI pair programmer. Save conversations and continue any time. Support OpenAI, DeepInfra, AnyScale, TogetherAI and Ollama. Create new files, view diffs with one click; your copilot to learn code, add tests, find bugs and more. Generate commit messages using AI.


## Features

- **Code Generation**: Generate code based on your prompts and descriptions.
- **Testing**: Add tests, find bugs, and optimize your code with the help of AI.
- **Explanation**: Explain your code and get insights into its functionality.
- **Commenting**: Add comments to your code to improve readability.
- **Commit Message Generation**: Generate commit messages using AI based on your git diff.
- **Conversation History**: Save and load conversations for future reference.
- **Custom Prompts**: Create and use custom prompts for specific tasks.
- **Ad-hoc Commands**: Use ad-hoc commands for quick actions.
- **Bot Name**: Rename and personalize your assistant.
- **Tokens**: Streaming conversation support and stop the response to save your tokens.
- **Export History**: Export all your conversation history at once in Markdown format.


## Requirements

- Visual Studio Code version 1.80.0 or higher
- Api Keys from OpenAi, DeepInfra, AnyScale or TogetherAi


## Installation

1. Open Visual Studio Code
2. Go to the Extensions view (Ctrl+Shift+X)
3. Search for 'SpaceBoxAi'
4. Click on Install


## Demo
<p align="center">

### Custom prompt
<a href="images/bugs.gif" target="_blank"><img src="images/bugs.gif" width="900"></a>

### Git commit
<a href="images/commit.gif" target="_blank"><img src="images/commit.gif" width="900"></a>

### Ollama models
<a href="images/ollama.gif" target="_blank"><img src="images/ollama.gif" width="900"></a>

### Optimize code
<a href="images/optimize.gif" target="_blank"><img src="images/optimize.gif" width="900"></a>

### Chat
<a href="images/prompt.gif" target="_blank"><img src="images/prompt.gif" width="900"></a>
</p>


## Note

1. Remember to change the API key every time you change the API end-point URL. In the future, I might include a function to save the API for all providers.
2. All models included in the list should be working fine. However, as you know, you have the option to add custom models. Keep in mind that not all models may work as expected, so be sure to test them first before using them in the extension.



## Disclaimer and Credits
- There is no guarantee that the extension will continue to work as-is without any issues or side-effects. Please use it at your own risk.
- This extension never uses/stores your personally identifiable information.
